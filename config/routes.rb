Rails.application.routes.draw do
  root 'login#login'
  post 'login', to: 'login#logincheck'
  post 'new_acc', to: 'login#new_acc'

  get 'homepage', to: 'home#homepage'
  post 'new_lst', to: 'home#new_lst'
  get 'del_lst', to: 'home#del_lst'

  get 'listview', to: 'listview#listview'
  post 'new_prod', to: 'listview#new_prod'
  get 'del_prod', to: 'listview#del_prod'
  get 'change_prods', to: 'listview#change_prods'

  get 'contact', to: 'contact#contact'
  post 'sendmessage', to: 'contact#sendmessage'
end
