# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Clear all previous data from database
User.destroy_all

# Create Users
kieran = User.create(email: 'kieran@gmail.com', name: 'Kieran', password: 'pass1')
brandon = User.create(email: 'brandon@gmail.com', name: 'Brandon', password: 'pass2')
lerissa = User.create(email: 'lerissa@gmail.com', name: 'Lerissa', password: 'pass3')

# Create Lists
gear = List.create(name: 'Climbing Gear', user_id: kieran.id)
programs = List.create(name: 'Programs', user_id: kieran.id)

shoes = List.create(name: 'Shoes', user_id: brandon.id)
skate = List.create(name: 'Skateboard Stuff', user_id: brandon.id)

pens = List.create(name: 'Pens', user_id: lerissa.id)
ingredients = List.create(name: 'Baking Ingredients', user_id: lerissa.id)

# Create Products
Product.create(name: 'Chalk', quantity: 2, acquired: 1, list_id: gear.id)
Product.create(name: 'Shoes', quantity: 1, acquired: 1, list_id: gear.id)
Product.create(name: 'Quick draws', quantity: 10, acquired: 0, list_id: gear.id)

Product.create(name: 'RubyMine', quantity: 1, acquired: 1, list_id: programs.id)
Product.create(name: 'FlStudio', quantity: 1, acquired: 1, list_id: programs.id)
Product.create(name: 'Photoshop', quantity: 1, acquired: 0, list_id: programs.id)

Product.create(name: 'Air Force 1', quantity: 1, acquired: 1, list_id: shoes.id)
Product.create(name: 'Yeezy Boost', quantity: 1, acquired: 0, list_id: shoes.id)
Product.create(name: 'Kyrie 4', quantity: 1, acquired: 1, list_id: shoes.id)

Product.create(name: 'Wheel', quantity: 4, acquired: 1, list_id: skate.id)
Product.create(name: 'Griptape', quantity: 1, acquired: 1, list_id: skate.id)
Product.create(name: 'Skate Tool', quantity: 1, acquired: 0, list_id: skate.id)

Product.create(name: 'Crayola', quantity: 30, acquired: 1, list_id: pens.id)
Product.create(name: 'Bic', quantity: 3, acquired: 0, list_id: pens.id)
Product.create(name: 'PaperMate', quantity: 5, acquired: 1, list_id: pens.id)

Product.create(name: 'Flour', quantity: 2, acquired: 1, list_id: ingredients.id)
Product.create(name: 'Banana', quantity: 3, acquired: 0, list_id: ingredients.id)
Product.create(name: 'Icing Sheet', quantity: 1, acquired: 0, list_id: ingredients.id)
