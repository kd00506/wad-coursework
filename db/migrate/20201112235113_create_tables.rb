class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :users, index: true do |t| # Users table
      t.string :email, null: false, unique: true
      t.string :name, null: false
      t.string :password, null: false

      t.timestamps
    end

    create_table :lists, index: true do |t| # Lists table
      t.string :name, null: false, unique: true
      t.belongs_to :user, foreign_key: true, null: false, index: false

      t.timestamps
    end

    create_table :products, index: true do |t| # Products table
      t.string :name, null: false
      t.integer :quantity
      t.boolean :acquired, null: false
      t.belongs_to :list, foreign_key: true, null: false

      t.timestamps
    end
  end
end
