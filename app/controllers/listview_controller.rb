class ListviewController < ApplicationController

  def prod_params # Strong parameters for product objects
    params.permit(:list_id, :quantity, :acquired, :name, :product_id, quants: [], acqs: [], names: [], product_ids: [])
  end

  def change_prods # Method for changing specific values in the product object
    changes = Hash[] # Set up a hashmap to keep track of changes

    (0..prod_params[:product_ids].length - 1).each do |i| # Loop through each product and keep track of the relevant changes
      change = Hash[]

      unless prod_params[:quants].nil? || prod_params[:quants][i].blank? || Integer(prod_params[:quants][i]) < 1
        change[:quantity] = prod_params[:quants][i]
      end
      change[:acquired] = prod_params[:acqs][i] unless prod_params[:acqs].nil? || prod_params[:acqs][i].blank?
      change[:name] = prod_params[:names][i] unless prod_params[:names].nil? || prod_params[:names][i].blank?

      changes[Integer(prod_params[:product_ids][i])] = change # Add these changes to the original hashmap
    end

    Product.update(changes.keys, changes.values) # Apply all changes from hashmap to database and then reload the page
    redirect_to listview_path
  end

  def del_prod # Method for deleting a product
    Product.destroy(prod_params[:product_id]) # Destroy the product and reload page
    redirect_to listview_path
  end

  def listview # Method for displaying the list page
    if session[:user_id].nil?  # If we have no session variable for the user_id, then the user has not signed in
      redirect_to root_path
    else # Otherwise we set the relevant variables and render the listview page

      session[:list_id] = prod_params[:list_id] unless prod_params[:list_id].blank?

      @list_id = session[:list_id]

      @username = User.find(session[:user_id]).name
      @pagename = List.find(session[:list_id]).name

    end
  end

  def new_prod # Method for creating a new product
    valid_quantity = (!prod_params[:quantity].blank? && Integer(prod_params[:quantity]) >= 1) || prod_params[:quantity].blank?
    valid_name = !prod_params[:name].blank?

    if valid_quantity && valid_name # Check if the quantity and name are valid, if not, then we do nothing
      if Product.list(prod_params[:list_id]).where(name: prod_params[:name]).exists? # If a product in the list with the same name exists, just increment the quantity
        prod = Product.find_by(list_id: prod_params[:list_id], name: prod_params[:name])
        quant = !prod_params[:quantity].nil? && prod_params[:quantity].number? ? Integer(prod_params[:quantity]) : 1
        Product.update(prod.id, quantity: [1, prod.quantity].max + quant)
      else # Otherwise create a new product with the specified values
        @prod = Product.create(name: prod_params[:name], quantity: !prod_params[:quantity].blank? ? prod_params[:quantity] : 1, acquired: 0,
                               list: List.find(prod_params[:list_id]))
      end
    end
    redirect_to listview_path
  end
end

class String
  def number? # Method for checking if a string can parsed to a number
    true if Float(self)
  rescue StandardError
    false
  end
end
