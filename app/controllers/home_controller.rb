class HomeController < ApplicationController

  def list_params # Strong parameters for list objects
    params.permit(:name, :list_id)
  end

  def new_lst # Method for creating new lists
    if List.user(session[:user_id]).where(name: list_params[:name]).exists? # If the user already has a list of the same name, then we display a relevant message
      redirect_to homepage_path alert: t('.homepage.existing', name: params[:name])
    else # If there is no pre-existing list, then we create one
      user = User.find(session[:user_id])
      List.create(name: list_params[:name], user: user)
      redirect_to homepage_path
    end
  end

  def del_lst # Method for deleting a list
    List.destroy(list_params[:list_id]) unless List.find(list_params[:list_id]).blank? # If the list_id is not blank, then we delete the relevant list
    redirect_to homepage_path
  end

  def homepage # Method for displaying home page
    if session[:user_id].nil? # If we have no session variable for the user_id, then the user has not signed in
      redirect_to root_path # Redirect back to the login page
    else # Otherwise, we go ahead set the correct variables and show the homepage view
      user = User.find(session[:user_id])
      @user_id = user.id

      @username = user.name
      @pagename = t('.page_title')
    end
  end
end
