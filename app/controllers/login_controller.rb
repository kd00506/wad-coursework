class LoginController < ApplicationController

  def login_params # Strong parameters for list objects
    params.permit(:name, :email, :password)
  end

  def login # Method for login page view
  end

  def logincheck # Here we check if the login info is correct
    if login_params[:email].blank? || login_params[:password].blank? # If the user left any fields blank, then we redirect back to the login page and display a relevant message
      redirect_to root_path, alert: t('.login.blank')
    elsif User.where(email: login_params[:email], password: login_params[:password]).exists? # If we can find a user with a matching email and password
      session[:user_id] = User.find_by(email: login_params[:email]).id # Then we set the user_id session variable and then go to the homepage
      redirect_to homepage_path
    else # If we cannot find a matching user, then we redirect back to the login page and display the appropriate alert
    redirect_to root_path, alert: t('.login.incorrect')
    end
  end

  def new_acc # Method for handling creation of a new account
    if login_params[:email].blank? || login_params[:name].blank? || login_params[:password].blank? # If the user left fields blank, then we redirect to login page with a message
      redirect_to root_path, alert: t('.login.blank')
    elsif User.where(email: login_params[:email]).exists? # If the email is already taken, then redirect back to login page with the appropriate message
      redirect_to root_path, alert: t('.login.in_use')
    elsif login_params[:email] =~ URI::MailTo::EMAIL_REGEXP # If the email matches the standard email regex, then we create the account, and set the session variable
      session[:user_id] = User.create(email: login_params[:email], name: login_params[:name], password: login_params[:password]).id
      redirect_to homepage_path
    else # Otherwise the email was in an invalid faormat, so we go back to the login page and display a message
    redirect_to root_path, alert: t('.login.invalid')
    end
  end
end