class ContactController < ApplicationController

  def contact_params # Strong parameters for contact page
    params.permit(:email, :message)
  end

  def contact # Display the contact view
    if session[:user_id].blank? # If we have no session variable for the user_id, then the user has not signed in
      redirect_to root_path
    else # Otherwise, we set up the variables relevant to the page
      @username = User.find(session[:user_id]).name
      @pagename = t('.page_title')
      @email = User.find(session[:user_id]).email
    end
  end

  def sendmessage # Method for sending the message
    if contact_params[:email].blank? # If the email was blank, then stay on page and display a flash alert
      flash[:alert] = t('.contact.blank_email')
      redirect_to contact_path
    elsif contact_params[:message].blank? # If the message is blank, then stay on the page and display a flash alert
      flash[:alert] = t('.contact.blank_message')
      redirect_to contact_path
    elsif contact_params[:email] =~ URI::MailTo::EMAIL_REGEXP # If the email is in the correct format, then we send the message and go to home page
      ContactMailer.contact_email(contact_params[:email], contact_params[:message]).deliver_now
      redirect_to homepage_path
    else # Otherwise stay on page and display a flash alert
      flash[:alert] = t('.contact.invalid')
      redirect_to contact_path
    end
  end
end
