class ContactMailer < ApplicationMailer

  def contact_email(email, message) # Handling of email
    @email = email
    @message = message

    mail cc: email
  end
end
