class ApplicationMailer < ActionMailer::Base
  default to: 'admin@team.com', from: 'a@customer.com' # Mailer defaults
  layout 'mailer'
end
