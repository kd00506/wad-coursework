function removeList(id, elem) { // Method for removing a list
    getButton("/del_lst", "list_id", id);
    elem.closest(".listCon").remove();
}

function removeProd(id, elem) { // Method for removing a product
    getButton("/del_prod", "product_id", id);
    elem.closest(".prodCon").remove();
}

function newListCheck(elem) { // Method to disable the new list button depending on whether a text field is empty
    $("#newListSubmit").prop('disabled', elem.value.length === 0);
}