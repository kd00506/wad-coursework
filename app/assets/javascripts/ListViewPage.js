let nextChangeIds = [];
let nextChangeQuants = [];
let nextChangeAcqs = [];
let nextChangeNames = [];

let timer;

function performUpdate() { // The request is made inside of this function
    $.ajax({
        url: "/change_prods",
        data: {
            "product_ids": nextChangeIds,
            "quants": nextChangeQuants,
            "acqs": nextChangeAcqs,
            "names": nextChangeNames
        },
        type: "get"
    });

    nextChangeIds = [];
    nextChangeQuants = [];
    nextChangeAcqs = [];
    nextChangeNames = [];

    timer = null;
}

function updateProduct(field, id, col){ // This method will allow the user to make multiple changes at once, without sending multiple requests
    if(col === "name" && field.value.length === 0){ // If we are updating the name, but the user has emptied it, we do nothing
        return;
    } else if(col === "quantity" && field.value < 1){ // If we are updating the quantity, but the user has set it to less than 1, we do nothing
        return;
    }

    let present = false; // Check to see if we have already updated the current product, if so, then update the relevant array with the new data
    for(let i = 0; i < nextChangeIds.length; i++){
        if(nextChangeIds[i] === id){
            if(col === "quantity"){
                nextChangeQuants[i] = field.value;
            } else if(col === "acquired"){
                nextChangeAcqs[i] = field.checked ? 1 : 0;
            } else if(col === "name"){
                nextChangeNames[i] = field.value;
            }

            present = true;
            break;
        }
    }

    if(!present){ // If we did not already find the element in the sectino above, then create a new change
        nextChangeIds.push(id);

        if(col === "quantity"){
            nextChangeQuants.push(field.value);
            nextChangeAcqs.push("");
            nextChangeNames.push("");
        } else if(col === "acquired"){
            nextChangeQuants.push("");
            nextChangeAcqs.push(field.checked ? 1 : 0);
            nextChangeNames.push("");
        } else if(col === "name"){
            nextChangeQuants.push("");
            nextChangeAcqs.push("");
            nextChangeNames.push(field.value);
        }
    }

    if(!timer){ // This timer makes sure that a request is not sent more than every so milliseconds
        timer = setTimeout(performUpdate, 2000);
    }
}