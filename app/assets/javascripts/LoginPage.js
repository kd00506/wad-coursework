function showLoginForm() { // Hide the create form and show the login form
    $("#loginForm").css("display", "flex");
    $("#createForm").css("display", "none");
}

function showCreateForm() { // Hide the login form and show the create form
    $("#loginForm").css("display", "none");
    $("#createForm").css("display", "flex");
}