class Product < ApplicationRecord
  belongs_to :list # Every product belongs to a list
  validates :name, presence: true # A product should not exist without a name
  validates :acquired, inclusion: {in: [true, false]} # The "acquired" field of a product should be either true or false
  validates :list_id, presence: true # A product should not exist without reference to its parent list

  scope :list, ->(list_id) { where(list_id: list_id) } # A scope that returns all products in a specified list
end
