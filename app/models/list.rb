class List < ApplicationRecord
  belongs_to :user # A list is owned by a user
  validates :name, presence: true # A list should not exist without a name
  validates :user_id, presence: true # A list should not exist without an id reference to its user

  has_many :products, dependent: :destroy # Each list owns a collection of products

  scope :user, ->(user_id) { where(user_id: user_id) } # A scope that only returns the lists owned by a user of specified user_id
end
