class User < ApplicationRecord
  validates :name, presence: true # A user should not exist without a name
  validates :email, presence: true, uniqueness: true # A user should not exist without a unique email address
  validates :password, presence: true # A user should not exist without a password

  has_many :lists, dependent: :destroy # Each user owns a collection of lists
end
