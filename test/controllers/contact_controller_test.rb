require 'test_helper'

class ContactControllerTest < ActionDispatch::IntegrationTest
  test 'should get contact' do
    post new_acc_path, params: {name: 'Ben', email: 'email@address.com', password: 'bing'}
    get contact_path
    assert_response :success
  end

  test 'should be logged in to get contact' do
    get contact_path
    assert_redirected_to root_path
  end

  test 'email should not be blank and should be in a valid format' do
    post sendmessage_path, params: {email: 'email@address.com', message: 'boop'} # Correct
    assert_redirected_to homepage_path

    post sendmessage_path, params: {email: 'bad_email', message: 'boop'} # Incorrect
    assert_redirected_to contact_path

    post sendmessage_path, params: {email: '', message: 'boop'} # Incorrect
    assert_redirected_to contact_path
  end

  test 'message should not be blank' do
    post sendmessage_path, params: {email: 'email@address.com', message: 'boop'} # Correct
    assert_redirected_to homepage_path

    post sendmessage_path, params: {email: 'email@address.com', message: ''} # Incorrect
    assert_redirected_to contact_path
  end
end
