require 'test_helper'

class LoginControllerTest < ActionDispatch::IntegrationTest
  test 'should get login/root' do
    get root_path
    assert_response :success
  end

  test 'login email should not be blank' do
    post login_path, params: {email: '', password: 'bing'}
    assert_redirected_to root_path
  end

  test 'login password should not be blank' do
    post login_path, params: {email: 'email@address.com', password: ''}
    assert_redirected_to root_path
  end

  test 'new account name should not be blank' do
    post new_acc_path, params: {name: '', email: 'email@address.com', password: 'bing'}
    assert_redirected_to root_path
  end

  test 'new account email should not be blank and should be in correct format' do
    post new_acc_path, params: {name: 'Ben', email: '', password: 'bing'}
    assert_redirected_to root_path

    post new_acc_path, params: {name: 'Ben', email: 'bad_email', password: 'bing'}
    assert_redirected_to root_path
  end

  test 'new account password should not be blank' do
    post new_acc_path, params: {name: 'Ben', email: 'email@address.com', password: ''}
    assert_redirected_to root_path
  end
end
