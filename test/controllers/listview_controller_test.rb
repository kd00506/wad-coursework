require 'test_helper'

class ListviewControllerTest < ActionDispatch::IntegrationTest

  test 'should get listview' do
    post new_acc_path, params: {name: 'Ben', email: 'email@address.com', password: 'bing'}
    get listview_path params: {list_id: List.first.id}
    assert_response :success
  end

  test 'should be logged in to get listview' do
    get listview_path
    assert_redirected_to root_path
  end

  test 'new product quantity should be empty or greater than 0' do
    assert_difference('Product.count', 0) do
      post new_prod_path, params: {name: 'food', quantity: -4, list_id: List.first.id}
      assert_redirected_to listview_path
    end

    assert_difference('Product.count') do
      post new_prod_path, params: {name: 'food', quantity: 3, list_id: List.first.id}
      assert_redirected_to listview_path
    end

    assert_difference('Product.count', 0) do
      post new_prod_path, params: {name: 'food', list_id: List.first.id}
      assert_redirected_to listview_path
    end
  end

  test 'new product name should not be empty' do

    assert_difference('Product.count') do
      post new_prod_path, params: {name: 'food', quantity: 3, list_id: List.first.id}
      assert_redirected_to listview_path
    end

    assert_difference('Product.count', 0) do
      post new_prod_path, params: {quantity: 3, list_id: List.first.id}
      assert_redirected_to listview_path
    end
  end

  test 'product quantity should be empty or greater than 0' do
    post new_prod_path, params: {name: 'food', quantity: 4, list_id: List.first.id}

    get change_prods_path, params: { product_ids: [Product.first.id], quants: [69] } # Correct
    assert Product.first.quantity = 69
    assert_redirected_to listview_path

    get change_prods_path, params: { product_ids: [Product.first.id], quants: [''] } # Incorrect
    assert Product.first.quantity = 69
    assert_redirected_to listview_path

    get change_prods_path, params: { product_ids: [Product.first.id], quants: [-4] } # Incorrect
    assert Product.first.quantity = 69
    assert_redirected_to listview_path
  end

  test 'product name should not be empty' do
    get change_prods_path, params: { product_ids: [Product.first.id], names: ['nothing'] } # Correct
    assert Product.first.name = 'nothing'
    assert_redirected_to listview_path

    get change_prods_path, params: { product_ids: [Product.first.id], names: [''] } # Incorrect
    assert Product.first.name = 'nothing'
    assert_redirected_to listview_path
  end
end
