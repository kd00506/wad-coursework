require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test 'should get home' do
    post new_acc_path, params: {name: 'Ben', email: 'email@address.com', password: 'bing'}
    get homepage_path
    assert_response :success
  end

  test 'should be logged in to get home' do
    get homepage_path
    assert_redirected_to root_path
  end
end
