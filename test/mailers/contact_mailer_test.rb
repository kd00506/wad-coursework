require 'test_helper'

class ContactMailerTest < ActionMailer::TestCase
  test 'should return contact email' do
    mail = ContactMailer.contact_email('kieran@dore.com', "what's up bro")
    assert_equal ['admin@team.com'], mail.to
    assert_equal ['a@customer.com'], mail.from
  end
end
